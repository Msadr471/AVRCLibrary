# AVRCLibrary

Overview:
----------
This manual is designed to help embedded programmers and students, rapidly exploit the Avr(Atmega)-Controller for embedded applications.
This manual has been targeted at embedded systems programmers and Students who have basic knowledge of Avr(Atmega32/Avr) architecture and C-Language.

This manual provides the reference to all the library functions which are grouped under respective.c file. The .c files convention is as per the peripherals. The peripherals (lcd, keypad..) are connected to default PORTs which can be connect to required PORTs by changing the #defines .

Reference:
----------
It is recommended to go through the below reference documents and datasheets before interfacing any peripherals.

The Avr Microcontroller and Embedded Systems by Muhammad Ali Mazidi.
Atmega32 DataSheet.
Embedded C by Michael J Pont .
Any of the 16x2 lcd datasheet.
RTC-DS1307 from Dallas Semiconductors.

Feedback:
----------
Suggestions for additions and improvements in code and documentation are always welcome. Please send your feedback via e-mail to feedback@xplorelabz.com

Disclaimer:
-----------
The libraries have been tested for Atmega16 on different development boards. We strongly believe that the library works on any Atmega boards. However, Xplore Labz disclaims any kind of hardware failure resulting out of usage of libraries, directly or indirectly. Documentation may be subject to change without prior notice.

The usage of tools and software demonstrated in the document are for educational purpose only, all rights pertaining to these belong to the respective owners. Users must ensure license terms are adhered to, for any use of the demonstrated software.

GNU GENERAL PUBLIC LICENSE: The library code in this document is licensed under GNU General Public License (GPL) Copyright (C) 2012. Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed. Since the library is licensed free of charge, there is no warranty for the libraries and the entire risk of the quality and performance is with the user.

Errors and omissions should be reported to feedback@xplorelabz.com